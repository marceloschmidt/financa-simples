App.info({
	name: 'Finanças Simples',
	description: 'Meu aplicativo de finanças',
	version: '0.0.1',
	author: 'Diego Sampaio',
	email: 'chinello@gmail.com',
	website: 'http://chinello.net'
});

App.icons({
	'android_mdpi': 'icons/drawable-mdpi/ic_launcher.png',
	'android_hdpi': 'icons/drawable-hdpi/ic_launcher.png',
	'android_xhdpi': 'icons/drawable-xhdpi/ic_launcher.png',
	'android_xhdpi': 'icons/drawable-xxhdpi/ic_launcher.png',
	'android_xhdpi': 'icons/drawable-xxxhdpi/ic_launcher.png'
});