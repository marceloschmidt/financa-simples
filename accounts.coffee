Accounts.config
	forbidClientAccountCreation: true

Accounts.onCreateUser? (options, user) ->
	user.responsavel = true
	user.nome = _.capitalize user.username

	# We still want the default hook's 'profile' behavior.
	if options.profile
		user.profile = options.profile

	return user
