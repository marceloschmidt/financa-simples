Meteor.methods
	dadosGrafico: ->
		filter =
			vencimento:
				$gte: moment().startOf('year').toDate()
				$lte: moment().endOf('year').toDate()

		ret =
			meses: []
			despesas: {}
			receitas: {}
			saldo: {}

		Lancamentos.find(filter).forEach (lancamento) ->
			if lancamento.valor < 0
				tipo = 'despesas'
			else
				tipo = 'receitas'

			mesLanc = moment(lancamento.vencimento).month()

			unless ret[tipo]['mes' + mesLanc]
				ret[tipo]['mes' + mesLanc] = 0

			ret[tipo]['mes' + mesLanc] += lancamento.valor

			ret.meses = _.union ret.meses, [ parseInt(mesLanc) ]

			console.log 'somando', lancamento

		console.log 'retorno ->', ret

		return ret
