Meteor.methods
	salvarLancamento: (rawFormData, templateData) ->
		# console.log rawFormData
		# console.log 'salva', rawFormData, templateData

		Mesosphere.lancamentoForm.validate rawFormData, (errors, formFieldsObject) ->
			# console.log 'errors ->', errors
			unless errors
				# console.log 'pode salvar', formFieldsObject, templateData

				if templateData._id?
					return Lancamentos.update templateData._id,
						'$set':
							'responsavel': Meteor.users.findOne(formFieldsObject.responsavel, { fields: {_id: 1, nome: 1 }})
							'descricao': formFieldsObject.descricao
							'valor': format.toNumber(formFieldsObject.valor)
							'vencimento': format.dateToMongo(formFieldsObject.vencimento)
				else
					console.log format.toNumber(formFieldsObject.valor)
					return Lancamentos.insert
						'responsavel': Meteor.users.findOne(formFieldsObject.responsavel, { fields: {_id: 1, nome: 1 }})
						'descricao': formFieldsObject.descricao
						'valor': format.toNumber(formFieldsObject.valor)
						'vencimento': format.dateToMongo(formFieldsObject.vencimento)
						'situacao': 'aberto'
			else
				return false
				# throw new Meteor.Error("pants-not-found", "Can't find my pants")
