Meteor.methods
	copiarLancamentos: (rawFormData, templateData) ->
		# console.log rawFormData
		# console.log 'salva', rawFormData, templateData

		Mesosphere.copiarLancamentosForm.validate rawFormData, (errors, formFieldsObject) ->
			# console.log 'errors ->', errors
			unless errors

				# console.log 'pode salvar', formFieldsObject, templateData

				dataInicio = moment(formFieldsObject['copiarde'], 'MM/YYYY')
				dataFim = moment(formFieldsObject['copiarde'], 'MM/YYYY').add(1, 'months').subtract(1, 'days')

				Lancamentos.find({ vencimento: { $gte: dataInicio.toDate(), $lte: dataFim.toDate }}).forEach (lancamento) ->
					delete lancamento._id
					lancamento.vencimento = moment(lancamento.vencimento).add(1, 'months').toDate()
					lancamento.situacao = 'aberto'

					Lancamentos.insert(lancamento)
			else
				return false
				# throw new Meteor.Error("pants-not-found", "Can't find my pants")
