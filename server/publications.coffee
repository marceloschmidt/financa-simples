Meteor.publish 'lancamentosDoMes', (data) ->
	unless this.userId
		return this.ready()

	filter =
		vencimento:
			$gte: new Date(data.getFullYear(), data.getMonth(), 1)
			$lte: new Date(data.getFullYear(), data.getMonth(), 31)

	return Lancamentos.find filter

Meteor.publish 'responsaveis', ->
	unless this.userId
		return this.ready()

	return Meteor.users.find { responsavel: true }, { fields: { 'nome': 1, 'responsavel': 1 }}

Meteor.publish 'userData', ->
	if this.userId
		return Meteor.users.find({_id: this.userId}, {fields: {'responsavel': 1}})
	else
		this.ready()
