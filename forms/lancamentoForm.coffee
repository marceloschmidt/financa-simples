Mesosphere
	name: 'lancamentoForm'
	template: 'lancamentoFormModal'
	# method: 'salvarLancamento'
	method: (rawFormData, templateData) ->
		# console.log 'sei la', arguments
		Mesosphere.lancamentoForm.validate rawFormData, (errors, formData) ->

		# console.log 'method.vv ->', vv

			unless errors
				Meteor.call 'salvarLancamento', formData, templateData, (errors, result) ->
					# console.log result

					Modal.hideNow()
					toastr.success('Lançamento salvo')

	onSuccess: (formData, formHandle) ->
		$(".has-error", formHandle).removeClass('has-error')
		Mesosphere.Utils.successCallback(formData, formHandle)

	# 	console.log 'success call()?', formData

	onFailure: (erroredFields, formHandle) ->
		formMarkErrorFields(erroredFields, formHandle)
		Mesosphere.Utils.failureCallback(erroredFields, formHandle)
	fields:
		responsavel:
			required: true
		descricao:
			required: true
		vencimento:
			required: true
		valor:
			required: true
