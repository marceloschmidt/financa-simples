Mesosphere
	name: 'copiarLancamentosForm'
	template: 'copiarLancamentosModal'
	# method: 'salvarLancamento'
	method: (rawFormData, templateData) ->
		# console.log 'sei la', arguments
		Mesosphere.copiarLancamentosForm.validate rawFormData, (errors, formData) ->

		# console.log 'method.vv ->', vv

			unless errors
				Meteor.call 'copiarLancamentos', formData, templateData, (errors, result) ->
					# console.log result

					Modal.hideNow()
					toastr.success('Lançamentos copiados')

	onSuccess: (formData, formHandle) ->
		$(".has-error", formHandle).removeClass('has-error')
		Mesosphere.Utils.successCallback(formData, formHandle)

	# 	console.log 'success call()?', formData

	onFailure: (erroredFields, formHandle) ->
		formMarkErrorFields(erroredFields, formHandle)
		Mesosphere.Utils.failureCallback(erroredFields, formHandle)
	fields:
		copiarde:
			required: true
			rules:
				mesPassado: 1
