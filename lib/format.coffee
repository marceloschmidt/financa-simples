@format =
	dateBR: (date) ->
		return _.lpad(date.getDate(), 2, '0') + '/' + _.lpad(date.getMonth() + 1, 2, '0') + '/' + date.getFullYear()

	dateToMongo: (date) ->
		if date.indexOf('/') is -1
			return null

		splitDate = date.split('/')
		return new Date(parseInt(splitDate[2]), parseInt(splitDate[1] - 1), parseInt(splitDate[0]))

	# format.toNumber('123,45') === 123.45 &&
	# format.toNumber('12.345') === 12345 &&
	# format.toNumber('12.345,67') === 12345.67 &&
	# format.toNumber('123') === 123 &&
	# format.toNumber(123) === 123 &&
	# format.toNumber(123.45) === 123.45 &&
	# format.toNumber(12345.67) === 12345.67
	toNumber: (number) ->
		unless _.isString number
			return number

		if number.indexOf(',') isnt -1
			return parseFloat(number.replace(/\./g, '').replace(',', '.'))
		else if number.indexOf('.') isnt -1
			return parseFloat(number.replace(/\./g, ''))
		else
			return parseInt(number)

