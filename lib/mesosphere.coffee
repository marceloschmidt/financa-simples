Mesosphere.registerRule 'mesPassado', (fieldValue, ruleValue) ->
	return moment(fieldValue, 'MM/YYYY').format('YYYY-MM-DD') < moment().date(1).format('YYYY-MM-DD')