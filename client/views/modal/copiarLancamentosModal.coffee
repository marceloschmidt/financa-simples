Template.copiarLancamentosModal.rendered = ->
	$('[name=copiarde]')
	.val(moment().subtract(1, 'months').format('MM/YYYY'))
	.datepicker
		format: "mm/yyyy"
		minViewMode: 1
		language: "pt-BR"
		autoclose: true
		todayHighlight: true
