Template.lancamentoDetalheModal.helpers
	dadosLancamento: ->
		return Lancamentos.findOne this.lancamentoId

	vencimento: ->
		return _.lpad(this.vencimento.getDate(), 2, '0') + '/' + _.lpad((this.vencimento.getMonth() + 1), 2, '0') + '/' + this.vencimento.getFullYear()

	valor: ->
		return _.numberFormat(this.valor, 2, ',', '.')

	naoPago: ->
		return this.situacao isnt 'pago'

	despesa: ->
		return this.valor <= 0

Template.lancamentoDetalheModal.events
	'click #btnExcluir': (e) ->
		e.preventDefault()
		e.stopPropagation()

		if confirm('Tem certeza que deseja excluir o lançamento: ' + this.descricao + '?')
			Meteor.call 'excluirLancamento', this._id, (errors, result) ->
				Modal.hideNow()
				toastr.success('Lançamento excluído')

	'click #btnAlterarLancamento': (e) ->
		e.preventDefault()
		e.stopPropagation()

		Modal.show('lancamentoFormModal', { lancamentoId: this._id })

	'click #btnDesfazerPagamento': (e) ->
		e.preventDefault()
		e.stopPropagation()

		Meteor.call 'desfazerPagamentoLancamento', this._id, (errors, result) ->
			# console.log result

			Modal.hideNow()
			toastr.success('Pagamento desfeito')

	'click #btnPagar': (e) ->
		e.preventDefault()
		e.stopPropagation()

		Meteor.call 'pagarLancamento', this._id, (errors, result) ->
			# console.log result

			Modal.hideNow()
			toastr.success('Lançamento pago')
