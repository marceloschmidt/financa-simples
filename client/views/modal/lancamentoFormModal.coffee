Template.lancamentoFormModal.helpers
	dadosLancamento: ->
		if this.lancamentoId
			return Lancamentos.findOne this.lancamentoId
		else
			return {}

	responsaveis: ->
		return Meteor.users.find { responsavel: true }, { sort: nome: 1}

	vencimento: ->
		if this.vencimento?
			return _.lpad(this.vencimento.getDate(), 2, '0') + '/' + _.lpad((this.vencimento.getMonth() + 1), 2, '0') + '/' + this.vencimento.getFullYear()

	valor: ->
		return _.numberFormat(this.valor, 2, ',', '.')

	responsavelSelected: (responsavelId, responsavelIdLancamento) ->
		if responsavelIdLancamento
			return 'selected' if responsavelId == responsavelIdLancamento

# Template.lancamentoFormModal.events
	# 'click #btnSalvarLancamento': (e) ->
	# 	e.stopPropagation()
	# 	e.preventDefault()

	# 	dadosLancamento = { hey: 'ok' }

	# 	console.log this

	# 	Meteor.call 'salvarLancamento', dadosLancamento, (error, result) ->
	# 		if (error)
	# 			alert('Erro: ' + error.reason)

	# 		alert('ok')


Template.lancamentoFormModal.rendered = ->
	$('[name=vencimento]').datepicker
		format: "dd/mm/yyyy"
		todayBtn: "linked"
		language: "pt-BR"
		autoclose: true
		todayHighlight: true
