Template.home.helpers
	responsaveis: ->
		return Meteor.users.find({responsavel: true})

	dataCorrente: ->
		return format.dateBR(Session.get('dataCorrente')).substr(3)

	despesas: (responsavelId, situacao) ->
		return Lancamentos.find({ 'responsavel._id': responsavelId, situacao: situacao, valor: { $lte: 0 } }, { sort: vencimento: 1, descricao: 1})

	despesasPagas: ->
		return Lancamentos.find({ situacao: 'pago', valor: { $lte: 0 } }, { sort: vencimento: 1, descricao: 1})

	receitas: ->
		return Lancamentos.find({ valor: { $gt: 0 } }, { sort: vencimento: 1, descricao: 1})

	despesaTotalResponsavel: (responsavelId) ->
		total = 0
		Lancamentos.find({ 'responsavel._id': responsavelId, situacao: 'aberto', valor: { $lte: 0 } }).forEach (l) ->
			total += l.valor

		return _.numberFormat(Math.abs(total), 2, ',', '.')

	despesaTotalPagas: ->
		total = 0
		Lancamentos.find({ situacao: 'pago', valor: { $lte: 0 } }).forEach (l) ->
			total += l.valor

		return _.numberFormat(Math.abs(total), 2, ',', '.')

	despesaTotalNaoPagas: ->
		total = 0
		Lancamentos.find({ situacao: 'aberto', valor: { $lte: 0 } }).forEach (l) ->
			total += l.valor

		return _.numberFormat(Math.abs(total), 2, ',', '.')

	receitasTotal: ->
		total = 0
		Lancamentos.find({ valor: { $gt: 0 } }).forEach (l) ->
			total += l.valor

		return _.numberFormat(Math.abs(total), 2, ',', '.')

Template.home.events
	'click #btnIncluirDespesa': (e) ->
		Modal.show('lancamentoFormModal')

	'click #btnMesAnterior': (e) ->
		e.currentTarget.blur()

		dataAtual = Session.get('dataCorrente')
		dataAtual.setMonth(dataAtual.getMonth() - 1)

		Router.go('mes', { mes: moment(dataAtual).format('YYYY-MM') })

	'click #btnProximoMes': (e) ->
		e.currentTarget.blur()

		dataAtual = Session.get('dataCorrente')
		dataAtual.setMonth(dataAtual.getMonth() + 1)

		Router.go('mes', { mes: moment(dataAtual).format('YYYY-MM') })


Template.home.rendered = ->
	$('#btnDataCorrente').datepicker
		format: "mm/yyyy"
		todayBtn: "linked"
		minViewMode: 1
		language: "pt-BR"
		autoclose: true
		todayHighlight: true
	.on 'changeMonth', (e) ->
		Router.go('mes', { mes: moment(e.date).format('YYYY-MM') })
		# Session.set('dataCorrente', e.date)
		# console.log e

	$('#btnDataCorrente').datepicker('update', Session.get('dataCorrente'))
