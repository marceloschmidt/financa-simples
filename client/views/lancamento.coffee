Template.lancamento.helpers
	vencimento: ->
		return _.lpad(this.vencimento.getDate(), 2, '0') + '/' + _.lpad((this.vencimento.getMonth() + 1), 2, '0') + '/' + this.vencimento.getFullYear()

	valor: ->
		return _.numberFormat(Math.abs(this.valor), 2, ',', '.')

	alertaVencimento: ->
		if this.valor > 0 or this.situacao isnt 'aberto'
			return ''

		avencer = this.vencimento - new Date()

		if avencer < 0 or avencer < (1000 * 60 * 60 * 24)
			return 'bg-danger'
		else if (1000 * 60 * 60 * 24) < avencer < (1000 * 60 * 60 * 24 * 4)
			return 'bg-warning'

Template.lancamento.events
	'click tr.lancamento': ->
		Modal.show 'lancamentoDetalheModal', { lancamentoId: this._id }
