Template.graficos.rendered = ->

	Meteor.call 'dadosGrafico', (error, result) ->
		# console.log result

		# console.log result.meses, result.meses.sort()

		meses = []
		receitas = []
		despesas = []
		saldos = []

		mesesOrdenados = result.meses.sort (a, b) ->
			return a - b

		for mes in mesesOrdenados
			# meses.push moment().month(mes).format('MMMM')

			if result.receitas['mes' + mes] or result.despesas['mes' + mes]
				if result.receitas['mes' + mes]
					receita = result.receitas['mes' + mes]
				else
					receita = 0

				if result.despesas['mes' + mes]
					despesa = result.despesas['mes' + mes]
				else
					despesa = 0

				mesAtual = moment().month(mes).format('MMMM')

				receitas.push
					label: mesAtual
					value: receita

				despesas.push
					label: mesAtual
					value: despesa

				saldos.push
					label: mesAtual
					value: receita + despesa

		data = [
			{
				"key": "Receitas",
				"color": "#00AA00",
				"values": receitas
			},
			{
				"key": "Despesas",
				"color": "#AA0000",
				"values": despesas
			},
			{
				"key": "Saldo",
				"color": "#0000AA",
				"values": saldos
			}
		]

		nv.addGraph ->
			chart = nv.models.multiBarChart()
					.x( (d) ->
						return d.label
					)
					.y((d) ->
						return d.value
					)
					# .margin({top: 30, right: 20, bottom: 50, left: 175})
					# .showValues(true)           #Show bar value next to each bar.
					.tooltips(true)             #Show tooltips on hover.
					.transitionDuration(350)
					.showControls(false)         #Allow user to switch between "Grouped" and "Stacked" mode.

			chart.yAxis.tickFormat(d3.format(',.2f'))

			d3.select('#myChart svg').datum(data).call(chart)

			nv.utils.windowResize(chart.update)

			return chart


		console.log data

