Template.header.helpers
	rotaAtual: (rota) ->
		return 'active' if rota is Router.current().route.getName()

Template.header.events
	'click #copiarLancamentos': (e) ->
		e.stopPropagation()
		e.preventDefault()

		Modal.show('copiarLancamentosModal')