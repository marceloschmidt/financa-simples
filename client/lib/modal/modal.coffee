@Modal =
	openedModalView: null
	openedModal: null
	callModalOpen: true
	show: (name, data) ->
		timeout = 1

		if @openedModalView?
			Modal.hideNow()
			timeout = 400

		data =
			template: name
			data: data

		self = this

		setTimeout ->
			self.openedModalView = Blaze.renderWithData Template.customModal, data, document.body
			self.openedModal = name
		, timeout

	hide: ->
		Blaze.remove @openedModalView

		@openedModalView = null
		@openedModal = null

	hideNow: ->
		# console.log 'hideNow', @openedModal
		$('#' + @openedModal).modal('hide')

Template.customModal.rendered = ->
	if Modal.callModalOpen
		templateName = this.data.template

		$('#' + templateName).modal()

		$('#' + templateName).on 'hidden.bs.modal', (e) ->
			Modal.hide()
