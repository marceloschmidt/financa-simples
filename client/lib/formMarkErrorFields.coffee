@formMarkErrorFields = (erroredFields, formHandle) ->
	$(".has-error", formHandle).removeClass('has-error')
	if erroredFields
		for campo, error of erroredFields
			$("[name=#{campo}]", formHandle).parents('.form-group').addClass('has-error')
