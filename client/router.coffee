# router

Router.configure
	layoutTemplate: 'masterLayout'
	progressDebug: false

Router.route '/',
	name: 'index'
	action: ->
		this.render 'home'
	waitOn: ->
		return [
			Meteor.subscribe('lancamentosDoMes', Session.get('dataCorrente')),
			Meteor.subscribe('responsaveis')
		]

Router.route '/mes/:mes',
	name: 'mes'
	action: ->
		console.log moment(this.params.mes, 'YYYY-MM').toDate()
		Session.set('dataCorrente', moment(this.params.mes, 'YYYY-MM').toDate())
		this.render 'home'
	waitOn: ->
		return [
			Meteor.subscribe('lancamentosDoMes', Session.get('dataCorrente')),
			Meteor.subscribe('responsaveis')
		]

Router.route '/graficos',
	name: 'graficos'
	action: ->
		this.render 'graficos'
	waitOn: ->
		return [
			Meteor.subscribe('lancamentosDoMes', Session.get('dataCorrente')),
			Meteor.subscribe('responsaveis')
		]
